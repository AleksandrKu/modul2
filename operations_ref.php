<?php
$monthArray = array(
    "01" => "январь",
    "02" => "февраль",
    "03" => "март",
    "04" => "апрель",
    "05" => "май",
    "06" => "июнь",
    "07" => "июль",
    "08" => "август",
    "09" => "сентябрь",
    "10" => "октябрь",
    "11" => "ноябрь",
    "12" => "декабрь");
$weekArray = array("", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье");
$text = "PHP может все. Главная область применения PHP - написание скриптов, работающих на стороне сервера; таким образом, PHP способен выполнять все.";
//$text = "PHP может все. Главная  ";  // for testing

// 1. Если в строке есть плохое слово (например, "редиска"), выведите предупреждение.
$badWord = 'PHP';  //
echo $text;
echo PHP_EOL.PHP_EOL;
// Perform a regular expression match. Search a word $badWord
//echo $print = (preg_match("/$badWord/", $text)) ? "Text have word '{$badWord}'\n\n " : "Text doesn't have word '{$badWord}'\n\n";
//var_dump(strpos($text,$badWord));  // если есть то возвращает позицию этого элемент
if (strpos($text,$badWord) !== false) { echo "Text have word '{$badWord}'\n\n "; } else { echo "Text doesn't have word '{$badWord}'\n\n"; }

// 2. Если в строке есть плохое слово (например, "редиска"), замените его на "***".
echo $newText = str_replace($badWord, "...", $text); // Replace all wolds $badWord on the string '...'

// 3. Если строка превышает 50 символов, выведите первые 50 символов и троеточие. Если строка меньше 50 символов, оставьте ее неизменной.
/*$length = strlen($text);  // String length
if ($length > 50) { // If more than 50 characters then cut the string
$newShotText = str_split($text, 50); // From string make array with value  with 50 characters
 $array = explode(" ",$newShotText[0]);  // First value of the array split into words
 $number = count($array); // Count how many words in this array
unset($array[$number-1]); // Unset last word . High probability that the word is trimmed.
array_push($array, ' ...'); // Add '...' in the and of array.
echo PHP_EOL.PHP_EOL.$newShotTextEnd = implode(" ", $array);  //  Join array elements with a string.
} else {
    $newShotTextEnd = $text;
    echo PHP_EOL.PHP_EOL.$newShotTextEnd;  // If text less than 50 characters just display.
}*/

$newtext2 = wordwrap($text, 50, " ... ");
var_dump($newtext2);
echo PHP_EOL.substr($newtext2,0,41);


//4. Проверьте, является ли введенная строка валидным email
// $email = $newShotTextEnd;
$email = "kkkkkk@ukr.n.net";
if (preg_match("[@]", $email) && preg_match("[\.]", $email)) {  // Check '@'  and '.' characters in the email
    print "\n\n'{$email}' - valid email ";
} else {
    print "\n\n '{$email}' - invalid email ";
}
echo PHP_EOL.PHP_EOL;
if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
	echo("$email is a valid email address");
} else {
	echo("$email is not a valid email address");
}


// Выведите текущую дату в формате: Четверг, 2 февраля, 2017 год.
echo PHP_EOL.PHP_EOL;
$weekId = date("w");
$week = $weekArray[$weekId];
$monthId = date("m");
$month = $monthArray[$monthId];
$year = date("o");
$day = date("j");
echo " {$week}, {$day} {$month}, {$year} год.";
