<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<style>
		table{
			width: 500px;
			border-spacing: 1px;
			border-color: black;
		}
		td, th {
			border: 1px solid black;
			text-align: center;
		}
		.month {
			color: black;
			text-align: center;
		}
			</style>
	<title>Calendar</title>
</head>
<body>
<?php
// * Student should create function getMonthName witch will accept parameter with month number and return name of needed month
// * Student should create function getWeekDays witch will return array with relation of numbers and names of days
// * Student should create function getMonthCalendar witch will accept parameters with number of month and year
// and return array of arrays of weeks with days in the current month
// * Student should create function buildCalendarTable witch will call function getMonthCalendar
// and convert result to user-friendly table like this:
// Students may to try to perform a task in different method and execute the program
function dayweekFistDayOfMonth() {
    $monthString = date("m");  // current month in format with 2 characters  01 - 09
    if ($monthString[0] == 0) { // if number of month 01 - 09  then take only second character
        $monthNumber = $monthString[1];
    } else {
        $monthNumber = $monthString;// if month from 10 to  12
    }
    $weekdayStartMonth = date("w", mktime(0, 0, 0, $monthNumber, 1, 2017)); // return number of weekday 1-th of current Month
    return $weekdayStartMonth;
}

// current month and year
function  getMonthName() {
return date('F Y');
}
// days of the week
function getWeekDays($w) {
    $weekDays = array('Monday',  'Tuesday', 'Wednesday',
        'Thursday', 'Friday', 'Saturday', 'Sunday');
    return $weekDays[$w];
}

// Month Calendar and build Table
function buildCalendarTable() {
    ?>
<table> <!-- create a table  -->
    <tr>
		<td class = 'month' colspan="7"> <?php echo getMonthName(); ?></td> <!-- display month and year  -->
    </tr>
<?php
$startDayweek = dayweekFistDayOfMonth(); // weekday the fist day of the month
$day = -6 - $startDayweek; // the cell where start print days
for ($i = 0; $i < 6; $i++) { // rows
    echo "<tr>";
    for ($j = 0; $j < 7; $j++) {  // cells in row
        $day = $day + 1; // day of month
        echo "<td>";
        if ($i == 0) { // the first row it is for names of the week
            echo getWeekDays($j);
        } else {
            if (($i == 1 && $j < $startDayweek - 1) || $day > 28) {  // in february 28 days
                echo " ";
            } else {
                echo "$day";
            }
        }
        echo "</td>";
    }
    echo "</tr>";
}
?> </table> <?
}
buildCalendarTable();
?>
</body>
</html>