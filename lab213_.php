<?php
/** Student should create Boolean variable with computer’s current statement (on/off).
 * Statement must be “off” by default
 * Student should create basic computer’s methods:
 * o start – prints welcome message and change statement to “on”
 * o shutdown – prints good buy message change statement to “off”
 * o restart – calls shutdown method, prints five dots with one second interval
 * and calls start method. If computer is off restart method must inform user about this issue
 * Student should call methods on the Computer object in next order:
 * o Start o Restart o Shutdown o Restart*/
class Computer2
{
	const IS_DESKTOP = true;
	var $cpu;
	var $ram;
	var $video;
	var $memory;
	var $isWorking = false; //Boolean variable with computer’s current statement (on/off)

	function start()
	{
		echo "Welcome user!\n";
		$this->isWorking = true;
	}

	function shutdown()
	{
		echo "Goodbuy!";
		$this->isWorking = false;
	}

function restart2()
{
	if ($this->isWorking) {
		$this->shutDown();
		for ($t = 1; $t <= 5; $t++) {
			echo '.';
			sleep(1);
		}
		echo PHP_EOL;
		$this->start();
	} else {
		echo "\nComputer must be turned on for restart" . PHP_EOL;
	}
}
}
$computer = new Computer2();
if ($computer instanceof Computer2) {
	$computer->start();
	sleep(1);
	$computer->restart2();
	sleep(1);
	$computer->shutDown();
	sleep(1);
	$computer->restart2();
}


